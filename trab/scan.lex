/* Coloque aqui definições regulares */



WS	[ \t\n]



D	[0-9]
L	[A-Za-z_]



INT	{D}+
FLOAT	{INT}("."{INT})?([Ee]("+"|"-")?{INT})?
ID	({L}|$|_)({L}|{D}|_)*



COM  ("/""*")([^*]|"*"[^/])*("*""/"){1,1}



STR1-S (\")([^(\")]|[\\][\"]|[\"][\"])*(\")
STR1-D (\')([^(\')]|[\\][\']|[\'][\'])*(\')
STR2   (`)[^`]*(`)



IF (i|I)(f|F)
FOR (f|F)(o|O)(r|R)




%%
    /* Padrões e ações. Nesta seção, comentários devem ter um tab antes */


{WS}	{ /* ignora espaços, tabs e '\n' */ } 







{IF}	{ return _IF; }
{FOR}	{ return _FOR; }



{INT}	{ return _INT; }
{FLOAT}	{ return _FLOAT; }


">="	{ return _MAIG; }
"<="	{ return _MEIG; }
"=="	{ return _IG; }
"!="	{ return _DIF; }



"//".*  { return _COMENTARIO; }
{COM}   { return _COMENTARIO; }


{STR1-S}  { return _STRING; }
{STR1-D}  { return _STRING; }
{STR2}    { return _STRING2; }



{ID}	{ return _ID; }



.       { return *yytext; 
          /* Essa deve ser a última regra. Dessa forma qualquer caractere isolado será retornado pelo seu código ascii. */ }





%%

/* Não coloque nada aqui - a função main é automaticamente incluída na hora de avaliar e dar a nota. */